import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../api/index';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';

const {height, width} = Dimensions.get('window');

const Profile = ({navigation}) => {
  const [userInfo, setUserInfo] = useState(null);
  const [userToken, setUserToken] = useState(null);

  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        setUserToken(token);
        // console.log('getToken -> token', token);
        // return getVenue(token);
      } catch (err) {
        console.log(err);
      }
    }
    getToken();
    getCurrentUser();
  }, [userInfo]);

  const getCurrentUser = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      // console.log('getCurrentUser->userInfo', userInfo);
      setUserInfo(userInfo);
    } catch (error) {
      console.log(error);
    }
  };

  const getVenue = (token) => {
    Axios.get(`${api}/venues`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
      },
    })
      .then((res) => {
        console.log('profile -> res', res);
      })
      .catch((err) => {
        console.log('profile -> err', err);
      });
  };

  const onLogoutPress = async () => {
    try {
      // await GoogleSignin.revokeAccess();
      // await GoogleSignin.signOut();
      // await AsyncStorage.removeItem('token');
      // navigation.navigate('Login');
      if (userInfo != null) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      }
      if (userToken != null) {
        await AsyncStorage.removeItem('token');
      }
      navigation.navigate('Login');
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#3EC6FF" barStyle="dark-content" />
      <View style={styles.boxUpper}>
        {/* <Image
          source={require('../../assets/images/profile.jpg')}
          style={{width: 80, height: 80, borderRadius: 40, marginBottom: 15}}
        /> */}
        <Image
          source={{uri: userInfo && userInfo.user && userInfo.user.photo}}
          style={{width: 80, height: 80, borderRadius: 40, marginBottom: 15}}
        />
        <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18}}>
          {/* Eko Praseyo */}
          {userInfo && userInfo.user && userInfo.user.name}
        </Text>
      </View>
      <View style={styles.boxUnder}>
        <View style={styles.boxRound}>
          <View style={styles.boxData}>
          <View style={styles.container}>
                <Button 
                style={styles.button}
                onPress={this.action}
                icon={require('./image/saldo')}
                iconSize={20}
                color={"white"}
                text={"Saldo       Rp.120.000.000"}
                />
            </View>
            <View style={styles.container}>
                <Button 
                style={styles.button}
                onPress={this.action}
                icon={require('./image/setting')}
                iconSize={20}
                color={"white"}
                text={"Pengaturan"}
                />
            </View>
            <View style={styles.container}>
                <Button 
                style={styles.button}
                onPress={this.action}
                icon={require('./image/bantuan')}
                iconSize={20}
                color={"white"}
                text={"Bantuan"}
                />
            </View>
            <View style={styles.container}>
                <Button 
                style={styles.button}
                onPress={this.action}
                icon={require('./image/syarat')}
                iconSize={20}
                color={"white"}
                text={"Syarat & ketentuan"}
                />
            </View>
            <View style={styles.container}>
                <Button 
                style={styles.button}
                onPress={this.action}
                icon={require('./image/keluar')}
                iconSize={20}
                color={"white"}
                text={"Keluar"}
                />
            </View>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => onLogoutPress()}
              style={styles.buttonLogout}>
              <Text
                style={{
                  backgroundColor: '#3EC6FF',
                  color: 'white',
                  height: 40,
                  paddingHorizontal: 25,
                  paddingVertical: 5,
                  borderRadius: 5,
                  width: width * 0.85,
                  textAlign: 'center',
                  fontSize: 20,
                }}>
                LOGOUT
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    backgroundColor: '#fff',
  },
  boxUpper: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3EC6FF',
    height: height * 0.33,
  },
  boxUnder: {
    backgroundColor: 'white',
  },
  boxData: {
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  boxRound: {
    backgroundColor: 'white',
    borderRadius: 8,
    elevation: 5,
    marginTop: -40,
    width: width * 0.9,
    alignSelf: 'center',
  },
  textDetail: {
    fontSize: 13,
    marginVertical: 7,
  },
  buttonLogout: {
    marginTop: 10,
    alignItems: 'center',
    marginBottom: 10,
  },
});

export default Profile;
