import React, {useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import styles from './style';
import {GiftedChat} from 'react-native-gifted-chat';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';

const Chat = ({navigation}) => {
  const [messages, setMessages] = useState([]);
  const [user, setUser] = useState({});

  useEffect(() => {
    const user = auth().currentUser;
    setUser(user);
    getData();
    return () => {
      const db = database().ref('messages');
      if (db) {
        db.off();
      }
    };
  }, []);

  const getData = () => {
    database()
      .ref('messages')
      .limitToLast(20)
      .on('child_added', (snapshot) => {
        const value = snapshot.val();
        setMessages((previousMessages) =>
          GiftedChat.append(previousMessages, value),
        );
      });
  };

  const onSend = (messages = []) => {
    for (let i = 0; i < messages.length; i++) {
      database().ref('messages').push({
        _id: messages[i]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: messages[i].text,
        user: messages[i].user,
      });
    }
  };

  return (
    <GiftedChat
      messages={messages}
      onSend={(messages) => onSend(messages)}
      user={{
        _id: user.uid,
        name: user.email,
        avatar:
          'https://store.playstation.com/store/api/chihiro/00_09_000/container/US/en/999/UP5588-CUSA16013_00-AV00000000000047/1580197046000/image?w=240&h=240&bg_color=000000&opacity=100&_version=00_09_000',
      }}
    />
  );
};

export default Chat;
