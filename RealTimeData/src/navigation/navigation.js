import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import SplashScreen from '../screens/Tugas5/Splashscreen';
import Intro from '../screens/Tugas5/Intro';
import Login from '../screens/Tugas5/Login';
import Profile from '../screens/Tugas2/Profile';
import Register from '../screens/Register/Register';
import Home from '../screens/Home/Home';
import Map from '../screens/Map/Map';
import Chart from '../screens/chat/chat';
import Chat from '../screens/Chat/Chat';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();

const TabsNavigation = () => (
  <Tabs.Navigator initialRouteName="Home">
    <Tabs.Screen
      name="Home"
      component={Home}
      options={{
        title: 'Home',
        tabBarIcon: ({color, size}) => (
          <MaterialCommunityIcons name="home" color={color} size={size} />
        ),
      }}
    />
    <Tabs.Screen
      name="Maps"
      component={Map}
      options={{
        title: 'Maps',
        tabBarIcon: ({color, size}) => (
          <MaterialCommunityIcons
            name="map-marker-radius"
            color={color}
            size={size}
          />
        ),
      }}
    />
    <Tabs.Screen
      name="Chat"
      component={Chat}
      options={{
        title: 'Chat',
        tabBarIcon: ({color, size}) => (
          <MaterialCommunityIcons name="chat" color={color} size={size} />
        ),
      }}
    />
    <Tabs.Screen
      name="Profile"
      component={Profile}
      options={{
        title: 'Profile',
        tabBarIcon: ({color, size}) => (
          <MaterialCommunityIcons
            name="account-box"
            color={color}
            size={size}
          />
        ),
      }}
    />
  </Tabs.Navigator>
);

const MainNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Intro"
      component={Intro}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Login"
      component={Login}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Profile"
      component={TabsNavigation}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Register"
      component={Register}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Home"
      component={TabsNavigation}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Chart"
      component={Chart}
      options={{
        title: 'React Native',
      }}
    />
    <Stack.Screen
      name="Chat"
      component={TabsNavigation}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

function AppNavigation() {
  const [isLoading, setIsLoading] = React.useState(true);

  //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 2000);
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  return (
    <NavigationContainer>
      <MainNavigation />
    </NavigationContainer>
  );
}

export default AppNavigation;
