import React, {useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../api/index';

const {height, width} = Dimensions.get('window');

const Profile = ({navigation}) => {
  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        console.log('getToken -> token', token);
        return getVenue(token);
      } catch (err) {
        console.log(err);
      }
    }
    getToken();
  }, []);

  const getVenue = (token) => {
    Axios.get(`${api}/venues`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
      },
    })
      .then((res) => {
        console.log('profile -> res', res);
      })
      .catch((err) => {
        console.log('profile -> err', err);
      });
  };

  const onLogoutPress = async () => {
    try {
      await AsyncStorage.removeItem('token');
      navigation.navigate('Login');
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.boxUpper}>
        <Image
          source={require('../../assets/images/ekoprasetyo.jpg')}
          style={{width: 80, height: 80, borderRadius: 40, marginBottom: 15}}
        />
        <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18}}>
          Eko Prasetyo
        </Text>
      </View>
      <View style={styles.boxUnder}>
        <StatusBar backgroundColor="#3EC6FF" barStyle="dark-content" />
        <View style={styles.boxRound}>
          <View style={styles.boxData}>
            <View>
              <Text style={styles.textDetail}>Tanggal Lahir</Text>
              <Text style={styles.textDetail}>Jenis Kelamin</Text>
              <Text style={styles.textDetail}>Hobi</Text>
              <Text style={styles.textDetail}>No. Telp</Text>
              <Text style={styles.textDetail}>Email</Text>
            </View>
            <View style={{alignItems: 'flex-end'}}>
              <Text style={styles.textDetail}>11 Oktober 1971</Text>
              <Text style={styles.textDetail}>Laki - laki</Text>
              <Text style={styles.textDetail}>Belajar</Text>
              <Text style={styles.textDetail}>08157646588</Text>
              <Text style={styles.textDetail}>eko.pcold@gmail.com</Text>
            </View>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => onLogoutPress()}
              style={styles.buttonLogout}>
              <Text
                style={{
                  backgroundColor: '#3EC6FF',
                  color: 'white',
                  height: 40,
                  paddingHorizontal: 25,
                  paddingVertical: 5,
                  borderRadius: 5,
                  width: width * 0.85,
                  textAlign: 'center',
                  fontSize: 20,
                }}>
                LOGOUT
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    backgroundColor: '#fff',
  },
  boxUpper: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3EC6FF',
    height: height * 0.33,
  },
  boxUnder: {
    backgroundColor: 'white',
  },
  boxData: {
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  boxRound: {
    backgroundColor: 'white',
    borderRadius: 8,
    elevation: 5,
    marginTop: -40,
    width: width * 0.9,
    alignSelf: 'center',
  },
  textDetail: {
    fontSize: 13,
    marginVertical: 7,
  },
  buttonLogout: {
    marginTop: 10,
    alignItems: 'center',
    marginBottom: 10,
  },
});

export default Profile;
