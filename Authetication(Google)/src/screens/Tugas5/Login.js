import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  Button,
  TextInput,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import styles from '../../style/style';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../api/index';
import auth from '@react-native-firebase/auth';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '600069288543-2lefm5s6mtriu3big3upl5tkm9os4kfb.apps.googleusercontent.com',
    });
  };

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      console.log('SignInWithGoogle -> idToken', idToken);
      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);
      navigation.navigate('Profile');
    } catch (error) {
      console.log('SignInWithGoogle -> error', error);
    }
  };

  const onLoginProcess = () => {
    let data = {
      email: email,
      password: password,
    };
    Axios.post(`${api}/login`, data, {
      timeout: 20000,
    })
      .then((res) => {
        console.log('login -> res', res);
        saveToken(res.data.token);
        navigation.navigate('Profile');
        setEmail('');
        setPassword('');
      })
      .catch((err) => {
        console.log('login -> err', err);
      });
  };

  return (
    <View style={styles.container}>
      <View style={{paddingTop: 10}}>
        <View style={{marginTop: 50, alignItems: 'center'}}>
          <Image
            source={require('../../assets/images/logo.jpg')}
            style={{height: 100, width: 250}}
          />
        </View>
        <View style={{alignItems: 'center', padding: 20}}>
          <Text style={[styles.textName, styles.label]}>Login</Text>
        </View>
        <View style={{paddingTop: 20}}>
          <View style={styles.formContainer}>
            <Text style={styles.label}>Username / Email</Text>
            <TextInput
              style={styles.input}
              value={email}
              underlineColorAndroid="#c6c6c6"
              placeholder="Username or Email"
              onChangeText={(email) => setEmail(email)}
            />
          </View>
          <View style={styles.formContainer}>
            <Text style={styles.label}>Password</Text>
            <TextInput
              style={styles.input}
              secureTextEntry
              value={password}
              underlineColorAndroid="#c6c6c6"
              placeholder="Password"
              onChangeText={(password) => setPassword(password)}
            />
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.btnLight}
              onPress={() => onLoginProcess()}>
              <Text style={{color: 'white', fontSize: 16}}>LOGIN</Text>
            </TouchableOpacity>

            <Text style={{color: 'black', fontSize: 16}}>OR</Text>
            {/* <TouchableOpacity style={styles.btnDark}>
              <Text style={{color: 'white', fontSize: 16}}>
                LOGIN WITH GOOGLE
              </Text>
            </TouchableOpacity> */}
            <GoogleSigninButton
              onPress={() => signInWithGoogle()}
              style={styles.btnGoogle}
              size={GoogleSigninButton.Size.Wide}
              color={GoogleSigninButton.Color.Dark}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default Login;
