import React, { createContext, useState } from "react";
import uuid from "uuid/v1";

export const TodoContext = createContext();

const TodoContextProvider = props => {
  const [todos, setTodos] = useState([]);

  const addTodo = todo => {
    setTodos([...todos, { todo, id: uuid() }]);
  };

  const removeTodo = id => {
    setTodos(todos.filter(todo => todo.id !== id));
  };
  
  return (
    <TodoContext.Provider value={{ todos, addTodo, removeTodo }}>
      {props.children}
    </TodoContext.Provider>
  );
};

export default TodoContextProvider;