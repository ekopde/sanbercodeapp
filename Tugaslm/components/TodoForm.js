
import React, { useState, useContext, useEffect } from "react";
import "../App.css";
import { TodoContext } from "../contexts/TodoContext";

export default function TodoForm() {
  
  const { todos, addTodo } = useContext(TodoContext);
  
  const [todo, setTodo] = useState("");

  const handleSubmit = e => {
    e.preventDefault();
    addTodo(todo);
    setTodo("");
  };

  return (
    <div className="card card-body my-3 form">
      <h3 className="text-center text-info"> Masukkan todolist</h3>
      <hr />
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          className="form-control"
          placeholder=""
          value={todo}
          required
          onChange={e => setTodo(e.target.value)}
        />
        <button className="btn btn-success btn-block mt-3">Add Todo</button>
      </form>
    </div>
  );
}