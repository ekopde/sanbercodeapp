import React from 'react';
import TextField from '@material-ui/core/TextField';
import Text from '@material-ui/core/DialogContentText';
import useInputState from './useInputState';

const TodoForm = ({ saveTodo }) => {
  const { value, reset, onChange } = useInputState();

  return (
    <form
      onSubmit={event => {
        event.preventDefault();

        saveTodo(value);
        reset();
      }}
    >
      <Text>Masukkan Todolist</Text>
     <View>
        <TextField
            variant="outlined"
            placeholder="Add todo"
            margin="normal"
            onChange={onChange}
            value={value}
        />
        <Button 
            icon={require('./image/plus')}
            iconSize={20}
            color={"white"}
        />
     </View>
     
    </form>
  );
};

export default TodoForm;