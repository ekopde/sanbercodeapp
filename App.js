/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

/* import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Header />
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>
            
            
            
            
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App; */
import React, {useEffect} from 'react';
import firebase from '@react-native-firebase/app';
import AuthenticationJWT from './src/navigation/navigation';

//ganti isi dari firebaseConfig, sesuai dengan project yang anda buat di firebase
var firebaseConfig = {
  apiKey: 'AIzaSyBw6Apm-9TFB1oqgZjQwhX0zqHtvzRnDsE',
  authDomain: 'sanbercode-3b002.firebaseapp.com',
  databaseURL: 'https://sanbercode-3b002.firebaseio.com',
  projectId: 'sanbercode-3b002',
  storageBucket: 'sanbercode-3b002.appspot.com',
  messagingSenderId: '600069288543',
  appId: '1:600069288543:web:72261ff564e3e7446421ea',
  measurementId: 'G-XBRKBPTTW3',
};
// Inisialisasi firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default function App() {
  // return <Intro />;
  // return <Profile />;
  // return <TodoList />;
  // return <Context />;
  // return <Splashscreen />;
  return <AuthenticationJWT />;
  return <
}
