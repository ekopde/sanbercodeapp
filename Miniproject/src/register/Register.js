import React, {useState} from 'react';
import {Image, Text, View, TouchableOpacity, Modal} from 'react-native';
import styles from './style';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import {RNCamera} from 'react-native-camera';
import storage from '@react-native-firebase/storage';

const Register = ({navigation}) => {
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);

  const toogleCamera = () => {
    setType(type === 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.takePictureAsync(options);
      console.log('takePicture -> data', data);
      setPhoto(data);
      setIsVisible(false);
    }
  };

  const uploadImage = (uri) => {
    const sessionId = new Date().getTime();
    return storage()
      .ref(`images/${sessionId}`)
      .putFile(uri)
      .then((response) => {
        alert(`Upload Success`);
        navigation.navigate('Login');
      })
      .catch((error) => {
        alert(error);
      });
  };

  const RenderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            ref={(ref) => {
              camera = ref;
            }}
            type={type}>
            <View style={styles.btnFlipContainer}>
              <TouchableOpacity
                style={styles.btnFlip}
                onPress={() => toogleCamera()}>
                <MaterialCommunityIcons name="rotate-3d-variant" size={15} />
              </TouchableOpacity>
            </View>
            <View style={styles.round} />
            <View style={styles.rectangle} />
            <View style={styles.btnTakeContainer}>
              <TouchableOpacity
                style={styles.btnTake}
                onPress={() => takePicture()}>
                <Feather name="camera" size={30} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <View style={styles.developerContainer}>
          <TouchableOpacity
            style={styles.developerContainer}
            onPress={() => {
              setIsVisible(true);
            }}>
            <Image
              style={styles.developerPhoto}
              source={
                photo === null
                  ? require('../../assets/images/blank.png')
                  : {uri: photo.uri}
              }
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={styles.loginButton}
          onPress={() => uploadImage(photo.uri)}>
          <Text style={styles.ButtonText}>UPLOAD PHOTO</Text>
        </TouchableOpacity>
      </View>
      <RenderCamera />
    </View>
  );
};

export default Register;
