import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  developerContainer: {
    marginTop: 20,
    backgroundColor: '#2ec1ac',
    width: Dimensions.get('window').width * 0.9,
    height: Dimensions.get('window').width * 1,
    borderRadius: 3,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  developerPhoto: {
    marginLeft: Dimensions.get('window').width * 0.1,
    alignSelf: 'center',
    width: Dimensions.get('window').width * 0.7,
    height: Dimensions.get('window').width * 0.7,
    borderRadius: Dimensions.get('window').width * 0.5,
  },
  body: {
    flex: 1,
  },
  loginButton: {
    marginTop: 20,
    width: Dimensions.get('window').width * 0.5,
    height: 40,
    backgroundColor: '#d2e603',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 8,
  },
  ButtonText: {
    fontSize: 16,
    alignSelf: 'center',
    color: '#FFFFFF',
  },

  btnFlipContainer: {
    margin: 10,
  },
  btnFlip: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  round: {
    width: Dimensions.get('window').height * 0.3,
    height: Dimensions.get('window').height * 0.45,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: '#fff',
    alignSelf: 'center',
  },
  rectangle: {
    marginTop: 50,
    width: Dimensions.get('window').width * 0.65,
    height: Dimensions.get('window').width * 0.4,
    borderWidth: 1,
    borderColor: '#fff',
    alignSelf: 'center',
  },
  btnTakeContainer: {
    alignItems: 'center',
  },
  btnTake: {
    marginTop: 20,
    height: 80,
    width: 80,
    borderRadius: 50,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
